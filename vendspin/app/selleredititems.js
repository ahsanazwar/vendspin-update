
import React from 'react';
import {
          View,
          Text,
          InteractionManager,
          ScrollView,
          StyleSheet,
          TouchableOpacity,
          Alert
       } from 'react-native';

import { Container, Content, Form, Item,
       Input, Button,StyleProvider,Header,Body,Right,Left,Icon,
       Title,List,ListItem,Thumbnail,ActionSheet,Badge } from 'native-base';

import Icon2 from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';
import Controller from './controller/controller';
var BUTTONS = [
  'Camera',
  'Gallery',
  'Cancel',
];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

export default class Selling extends React.Component {

  constructor(props) 
  {
        super(props);

        this.controller = new Controller();
          
      
        this.state = {
          renderPlaceholderOnly: true,
          dataNavigate:props.navigation.state.params.itemDetails,
          item_name:props.navigation.state.params.itemDetails.item_name,
          item_price:props.navigation.state.params.itemDetails.item_price,
          item_description:props.navigation.state.params.itemDetails.item_description,
          item_qty:props.navigation.state.params.itemDetails.item_qty,
          item_img:props.navigation.state.params.itemDetails.item_img,  
        };

        console.log("\n\n\n"+JSON.stringify(this.state.dataNavigate)+"\n\n\n"); 

      }
    pickSingleWithCamera(cropping) 
    {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
    }).then(image => {
      
      this.setState({
        image: {uri: image.path, width: image.width, height: image.height},
        images: null
      });
    }).catch(e => alert(e));
  }

  pickSingle(cropit, circular=false) {
    ImagePicker.openPicker({
      width: 70,
      height: 70,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      compressVideoPreset: 'MediumQuality',
    }).then(image => {
      
      this.setState({
        image: {uri: image.path, width: image.width, height: image.height, mime: image.mime},
        images: null
      });
    }).catch(e => {
      
      Alert.alert(e.message ? e.message : e);
    });
  }

  scaledHeight(oldW, oldH, newW) {
    return (oldH / oldW) * newW;
  }
  
   renderImage(image) {
    return <Thumbnail style={{width: 105, height: 105,
            borderWidth: 0.3,borderColor: '#cc0066'}} source={image} />
  }

  renderAsset(image) {
    if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
      return this.renderVideo(image);
    }

    return this.renderImage(image);
  }
      ////////////////////////////////////////////PlaceHolder//////////////////////////////////////////////
  
    
  static navigationOptions = {
    header: 
    {
      visible:true,

      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    },
    title: "Edit Items"
  };

  async componentDidMount() {
      this.str = await this.controller.getUserStr();
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

  isEmptyField()
    {
           if (this.state.item_name=="" && this.state.item_price=="")
           {
                return true;
           }
           {
                return false;
           }
    }

  async onSubmit()
      {
              if (this.isEmptyField()==true) 
                {
                      Alert.alert("Some fields are empty");
                } 
                else 
                {
                     var items = {};
                     items = {
                      item_id:this.state.dataNavigate.item_id,
                      item_name:this.state.item_name,
                      item_price:this.state.item_price,
                      item_description:this.state.item_description,
                      // item_img:this.state.item_img,
                      item_qty:this.state.item_qty,
                     };
                      
                      
                     var getData = await this.controller.sellerItem(items,this.str,1);
                     console.log(getData);
                     if(getData)
                     {  
                        if(getData.status)
                        {
                          Alert.alert(getData.msg);
                        }
                        else
                        {
                          Alert.alert(getData.msg);
                        }
                     }
                     else
                     {
                        Alert.alert("Something went wrong");
                     }
                     
                }
      }


    onDel()
    {   
      
      Alert.alert(
        'Are you sure you want to delete this item ?',
        null,
        [
          {text: 'No', onPress: () => {return false}},
          {text: 'Yes', onPress: async () => 
            {
             var items = {};
             items = {
                item_id:this.state.dataNavigate.item_id,
              }
              var getData = await this.controller.sellerItem(items,this.str,-1); 
                console.log(getData);
               if(getData)
               {  
                  if(getData.status)
                  {
                    Alert.alert(getData.msg);
                  }
                  else
                  {
                    Alert.alert(getData.msg);
                  }
               }
               else
               {
                  Alert.alert("Something went wrong");
               }
            }
          },
        ]);


    }  

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
         <Container style={{backgroundColor:'white'}}>
          <Content>
       
      <Container style={{flex:1,justifyContent:'center'}}>
              <Form>

             <Button style={{marginBottom:8,alignSelf:'center',height:105}} transparent
             onPress={()=> ActionSheet.show(
                  {
                    options: BUTTONS,
                    cancelButtonIndex: CANCEL_INDEX,
                    destructiveButtonIndex: DESTRUCTIVE_INDEX,
                    title: 'Profile Pic',
                  },
                  (buttonIndex) => {            
                    if (buttonIndex==0) 
                      {
                        this.pickSingleWithCamera(false)
                          
                      } 
                     else
                     if (buttonIndex==1) 
                     {
                        this.pickSingle(false)
                     } 
                  }
                  )}>
        
        {this.state.image ? this.renderAsset(this.state.image) : 
          <Thumbnail style={{width: 105, height: 105,borderWidth: 0.3,borderColor: '#cc0066'}} 
                            source={require('./img/defaultItem.jpg')} /> 
        }

        {this.state.images ? this.state.images.map(i => 
        <View key={i.uri}>{this.renderAsset(i)}</View>) : 
        null}

        <Badge style={{position:'absolute',bottom:0,right:18,backgroundColor:'#99004d'}}>
        <Icon2 name='camera' size={17} color="white" style={{flex: 1,justifyContent: 'center',marginTop:5}}/></Badge> 
      </Button>

                  <Item last>
                    <Input value={this.state.item_name}
                    onChangeText={(text) => this.setState({item_name: text})}/>
                </Item>

                  <Item last>
                      <Input value={this.state.item_price}
                      onChangeText={(text) => this.setState({item_price: text})}/>
                  </Item>
                  <Item last>
                      <Input value={this.state.item_description}
                      maxLength = {40}
                      multiline = {true}
                      numberOfLines = {4}
                      style={{"height":100}}
                      onChangeText={(text) => this.setState({item_description: text})}/>
                  </Item>
                  <Item last>
                      <Input value={this.state.item_qty}
                      onChangeText={(text) => this.setState({item_qty: text})}/>
                  </Item>
                  <Button 
                      style={{marginTop:30,alignSelf: 'center',width:170,
                                  backgroundColor:'#808080'}} 
                                  onPress={() => this.onSubmit()}>
                        <Text style={{fontSize:15,color:'#fff'}}>Update</Text>
                  </Button>
                  <Button 
                      style={{marginTop:30,alignSelf: 'center',width:170,
                                  backgroundColor:'#808080'}} 
                                  onPress={() => this.onDel()}>
                        <Text style={{fontSize:15,color:'#fff'}}>Delete</Text>
                  </Button>
            
              </Form>

              </Container>
          </Content>
      </Container>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};


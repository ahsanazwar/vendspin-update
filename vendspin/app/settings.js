
import React from 'react';
import {
  Text,
  View,
  TouchableHighlight,
  InteractionManager,
} from 'react-native';

import {Thumbnail,Icon,Button,List,ListItem,Card,CardItem} from 'native-base';

import Icon2 from 'react-native-vector-icons/FontAwesome';
import LocalImage from './LocalImage';
export default class Settings extends React.Component {

  constructor(props) {

        super(props);
        this.state = {
          renderPlaceholderOnly: true
        };
      }

/////////////////////////////////////PlaceHolder////////////////////////////////////
    
  static navigationOptions = {
    header: {visible:false},
    tabBar: {
            icon: ({ tintColor }) => {
                return (
                  <View>
                    <Icon2
                        name='bars'
                        size={27}
                        color={ "#fff" }
                    />
                  </View>  
                );

            }
        }
  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
        <View style={{flex: 1,backgroundColor: "#fff", height: 1000}}>

                <View>
                        <LocalImage 
                          source={require('./img/settings-banner.jpg')}
                          orignalWidth = {667}
                          orignalHeight = {400}
                          />
                </View>

                  <View>   
                      <List>
                        <ListItem last style={{height:50}}>
                            <Button transparent style={{alignSelf: 'stretch',flex: 1}}
                             onPress={() => navigate('buyerprofile')} 
                            ><Text style={{color:'#6699ff'}}>My Profile</Text></Button>
                        </ListItem>

                        <ListItem last style={{height:50}}>
                            <Button transparent style={{alignSelf: 'stretch',flex: 1}} onPress={() => navigate('sponsor')}><Text style={{color:'#6699ff'}}>Sponsor</Text></Button>
                        </ListItem>

                        <ListItem style={{height:50,paddingBottom:50}}>
                            <Button transparent style={{alignSelf: 'stretch',flex: 1}}><Text style={{color:'#6699ff'}}>Terms And Conditions</Text></Button>
                        </ListItem>        
                      </List>
                  </View>

                  <View>   
                      <List>
                        <ListItem last style={{height:50}}>
                            <Button transparent style={{alignSelf: 'stretch',flex: 1}}><Text
                            onPress= {() => navigate('root')}
                             style={{color:'#000000'}}>Logout</Text>
                             </Button>
                        </ListItem>        
                      </List>
                  </View>          
                 
                 </View>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};



import React from 'react';
import {
          View,
          Text,
          InteractionManager,
          ScrollView,
          StyleSheet,
          TouchableOpacity,
          Alert
       } from 'react-native';

import { Container, Content, Form, Item,
       Input, Button,StyleProvider,Header,Body,Right,Left,Icon,
       Title,List,ListItem,Thumbnail,ActionSheet,Badge } from 'native-base';

import Icon2 from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-crop-picker';

var BUTTONS = [
  'Camera',
  'Gallery',
  'Cancel',
];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

export default class Selling extends React.Component {

  constructor(props) {

        super(props);
        this.state = {
          renderPlaceholderOnly: true
        };
      }
    pickSingleWithCamera(cropping) 
    {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
    }).then(image => {
      
      this.setState({
        image: {uri: image.path, width: image.width, height: image.height},
        images: null
      });
    }).catch(e => alert(e));
  }

  pickSingle(cropit, circular=false) {
    ImagePicker.openPicker({
      width: 70,
      height: 70,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      compressVideoPreset: 'MediumQuality',
    }).then(image => {
      
      this.setState({
        image: {uri: image.path, width: image.width, height: image.height, mime: image.mime},
        images: null
      });
    }).catch(e => {
      
      Alert.alert(e.message ? e.message : e);
    });
  }

  scaledHeight(oldW, oldH, newW) {
    return (oldH / oldW) * newW;
  }
  
   renderImage(image) {
    return <Thumbnail style={{width: 105, height: 105,
            borderWidth: 0.3,borderColor: '#cc0066'}} source={image} />
  }

  renderAsset(image) {
    if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
      return this.renderVideo(image);
    }

    return this.renderImage(image);
  }
      ////////////////////////////////////////////PlaceHolder//////////////////////////////////////////////
  
    
  static navigationOptions = {
    header: 
    {
      visible:true,

      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    },
    title: ({state}) => `${state.params.item}`
  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
         <Container style={{backgroundColor:'white'}}>
          <Content>
       
      <Container style={{flex:1,justifyContent:'center'}}>
              <Form>

             <Button style={{marginBottom:8,alignSelf:'center',height:105}} transparent
             onPress={()=> ActionSheet.show(
                  {
                    options: BUTTONS,
                    cancelButtonIndex: CANCEL_INDEX,
                    destructiveButtonIndex: DESTRUCTIVE_INDEX,
                    title: 'Profile Pic',
                  },
                  (buttonIndex) => {            
                    if (buttonIndex==0) 
                      {
                        this.pickSingleWithCamera(false)
                          
                      } 
                     else
                     if (buttonIndex==1) 
                     {
                        this.pickSingle(false)
                     } 
                  }
                  )}>
        
        {this.state.image ? this.renderAsset(this.state.image) : 
          <Thumbnail style={{width: 105, height: 105,
            borderWidth: 0.3,borderColor: '#cc0066'}} source={require('./img/defaultItem.jpg')} /> 
        }

        {this.state.images ? this.state.images.map(i => 
        <View key={i.uri}>{this.renderAsset(i)}</View>) : 
        null}

        <Badge style={{position:'absolute',bottom:0,right:18,backgroundColor:'#99004d'}}>
        <Icon2 name='camera' size={17} color="white" style={{flex: 1,justifyContent: 'center',marginTop:5}}/></Badge> 
      </Button>

                  <Item last>
                        <Input placeholder='Title' />
                    </Item>
                  <Item last>
                      <Input placeholder="Description"/>
                  </Item>

                  <Item last>
                      <Input placeholder="Price" keyboardType = 'numeric' />
                  </Item>
                  <Button 
                            style={{marginTop:30,alignSelf: 'center',width:170,
                                  backgroundColor:'#808080'}} 
                                  onPress={() => alert(this.onSubmit())}>
                        <Text style={{flex: 1,textAlign:'center',fontSize:15,color:'#fff'}}>Add To Cart</Text>
                  </Button>
            
              </Form>

              </Container>
          </Content>
      </Container>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};


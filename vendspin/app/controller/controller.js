/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';
// import React,{Component} from 'react';
var ReactNative = require('react-native');

import storageModel from '../models/storageModel';
import registerModel from '../models/registerModel';
import sellerModel from '../models/sellerModel';
import buyerModel from '../models/buyerModel';
import cartModel from '../models/cartModel';

export default class Controller 
{
   
   
   constructor(  ) 
   {
   		this.storageModelCall = new storageModel();
   		this.registerModel = new registerModel();
      this.sellerModel = new sellerModel();
      this.buyerModel = new buyerModel();
      this.cartModel = new cartModel(this.storageModelCall);
   };

   getStatus()
   {
    return "Controller is active";
   };


   async auth()
   {    

     var user = await this.storageModelCall.getLocalData('user');
       
     if(user)
     {
       if(user.user_auth_key && user.user_type)
       {
        return user;
       }
       else
       {
        return false;
       }
     }
     else
     {
      return false;
     }
     
   }

  
   async getUserStr()
   {
    var getAuth = await this.auth();
    if(getAuth)
    { 
      var str = "?auth_key="+getAuth.user_auth_key+"&type="+getAuth.user_type;
      return str;
    }
    else
    {
      return false;
    }
    
   }

   // setProfile
   async setProfile(data)
   {  

       try
       {
         let getStatusUpdate =  await this.registerModel.postProfile(data,"signup")
         if(getStatusUpdate)
         {
           console.log("CONTROLLER ALL DATA() => () --> "+JSON.stringify(getStatusUpdate));
           if(getStatusUpdate.status == true)
           {  
              var setAuth = 
              {
                user_auth_key:getStatusUpdate.data.user_auth_key,
                user_type:getStatusUpdate.data.user_type
              } 
     
              console.log("CONTROLLER () => setAuth() "+setAuth.user_auth_key+" , "+setAuth.user_type)  
              await this.storageModelCall.setLocalData('user',setAuth);
              await this.storageModelCall.setLocalData('profile',getStatusUpdate);
           }
           return getStatusUpdate;
          }
          else
          {
           return false;
          }
       }
       catch(e)
       {
        return false;  
       }  
   }


   async login(data)
   {  
        let getStatusUpdate =  await this.registerModel.postProfile(data,"login")

        if(getStatusUpdate)
        {
          console.log("CONTROLLER ALL DATA() => () "+ getStatusUpdate)
          if(getStatusUpdate.status == true)
          {  
            var setAuth = 
            {
              user_auth_key:getStatusUpdate.data.user_auth_key,
              user_type:getStatusUpdate.data.user_type
            } 

            console.log("CONTROLLER () => setAuth() "+setAuth.user_auth_key+" , "+setAuth.user_type)

            await this.storageModelCall.setLocalData('user',setAuth);
            await this.storageModelCall.setLocalData('profile',getStatusUpdate);
          }

          return getStatusUpdate;
       }
       else
       {
        return false;
       }
   }


   async getUser(data)
   {  
        var profile = await this.registerModel.getUser(data);
        return profile;
   }




   async getLocalProfile()
   {  
        var profile = await this.storageModelCall.getLocalData("profile");
        return profile;
   }

   // getStoreId
   async getStoreId()
   {
     var getStoreId = await this.getLocalProfile();
     return getStoreId.data.store_id;
   }




   async updateUser(data,attr)
   {    
        var updateUser = await this.registerModel.updateUser(data,attr);
        if(updateUser)
        {
         return updateUser;
        }
        else
        {
          return false;
        }  
   }



   // seller
   async sellerItem(data,attr,type)
   {    
      if(type==0)
      {
        var addItem = await this.sellerModel.addItem(data,attr);
        if(addItem)
        {
         return addItem;
        }
        else
        {
          return false;
        }
      }

      else if(type==1)
      {
        var editItem = await this.sellerModel.editItem(data,attr);
        if(editItem)
        {
         return editItem;
        }
        else
        {
          return false;
        } 
      }  


      else if(type == -1)
      {
        var deleteItem = await this.sellerModel.deleteItem(data,attr);
        console.log(deleteItem+" here");
        if(deleteItem)
        {
         return deleteItem;
        }
        else
        {
          return false;
        } 
      }
      else
      {
        return false;
      }
   }




   async getStoreItems(data,attr)
   {    
        var getStoreItems = await this.sellerModel.getStoreItems(data,attr);
        
        if(getStoreItems)
        {
         return getStoreItems;
        }
        else
        {
          return false;
        }  
   }













   // buyer
   async getAllStore(data,attr)
   {
      var getAllStore = this.buyerModel.getAllStores(data,attr);
      if(getAllStore)
      {
       return getAllStore;
      }
      else
      {
        return false;
      } 
   }











   async getCart()
   {
     return await this.cartModel.Cart();
   }



   // addToCart
   async addToCart(data)
   {     
     return await this.cartModel.Cart(data); 
   }


   






   async clearData()
   {
      await this.storageModelCall.clearData();
   }
}


 

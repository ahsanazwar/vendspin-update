
import React from 'react';
import {TabNavigator} from 'react-navigation';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import {View,Text,InteractionManager,ScrollView
        , ListView, StyleSheet,Image} from 'react-native';

import MyStore from './mystore';
import Profile from './sellerProfile';
import MyOrder from './MyOrder'
import Settings from './settings';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
});
class Selling extends React.Component {

  constructor(props) {

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        super(props);
        this.state = {
          dataSource: ds.cloneWithRows(['row 1', 'row 2']),
          renderPlaceholderOnly: true
        };
      }


  static navigationOptions = {
    label: 'Home',
    header: {visible:false},
         tabBar: {
            label: '',
            icon: ({ tintColor }) => {
                return (
                  <View>
                    <Icon2
                        name='map'
                        size={27}
                        color={ "#fff" }
                    />
                  </View>  
                );

            }
        }
  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
    <ScrollView>
        <Text>Selling</Text>
    </ScrollView>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};

const selling = TabNavigator({
  "selling": {screen: Selling},
  "my store": {screen: MyStore},
  "my order": {screen: MyOrder},
  "Profile": {screen: Profile},
  "settings": {screen: Settings},
},
{
    tabBarOptions: {
    showLabel:false,
    showIcon: true,
    iconStyle: {
        width: 35,
        height: 30
    },
    pressColor: '#d5dcea',
    style:{backgroundColor:"#99004d"},
    indicatorStyle: {backgroundColor: '#ffb3ff'},
    labelStyle:{fontSize: 11},  
    }
},
);

export default selling;


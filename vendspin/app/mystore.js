import React, { Component, PropTypes } from 'react'
import {
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  InteractionManager,
  Alert
} from 'react-native';

import EasyListView from 'react-native-easy-listview-gridview';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import {Thumbnail,Fab,Icon} from 'native-base';
import Controller from './controller/controller';


const DATA_SIZE_PER_PAGE = 10

var data1,data2,data3;

export default class MyStore extends Component {
  mylist=[];
  iit=0;
  total=0;
  static navigationOptions = {
     header: {visible:false},
     tabBar: {
            label: '',
            icon: ({ tintColor }) => {
                return (
                  <View>
                    <Icon2
                        name='tag'
                        size={27}
                        color={ "#fff" }
                    />
                  </View>  
                );

            }
        }
  };
  static propTypes = {
    empty: PropTypes.bool,
    error: PropTypes.bool,
    noMore: PropTypes.bool,
    column: PropTypes.number
  };

  static defaultProps = {
    empty: false,
    error: false,
    noMore: false,
    column: 1
  }
      
  constructor(props) {
    super(props)
    console.log("working");
    this.controller = new Controller();
    
    this.renderListItem = this._renderListItem.bind(this)
    this.onFetch = this._onFetch.bind(this)
    this.state = {
          renderPlaceholderOnly: true
        };
  }

    async componentDidMount() {
      this.str = await this.controller.getUserStr();
      this.thisStoreId = 
      {
        store_id: await this.controller.getStoreId()
      }
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
      <View style={{position:'relative',flex: 1}}>
      <EasyListView 
        ref={component => this.listview = component}
        dataSizePerPage={DATA_SIZE_PER_PAGE}
        column={this.props.column}
        rowHeight={60}
        renderItem={this.renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetchfix}
        style={{marginTop:20}}
      />
      <Fab style={{backgroundColor:'#999999',position:'absolute'}}>          
          <Icon name="add" onPress= {() => navigate('additems')}/>
        </Fab>
      </View>
    )
  }
  _renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }

  _renderListItem(rowData, sectionID, rowID, highlightRow) {
     const { navigate } = this.props.navigation;
    
    return (
      <View style={{position:'relative',flex: 1,padding:4,flexDirection:'row'}}>

        <TouchableHighlight
          style={{flex: 1}}

          onPress= {() => navigate('selleredititems',
            {
                itemDetails:rowData,  
            }
          )}
          underlayColor="#fff">
          <View style={{flex: 1,borderBottomWidth:1, flexDirection: 'row'}}>
           
              <View>
           
               <Thumbnail style={{width: 80, height: 80, }} square 
               source={{uri:"http://drop.ndtv.com/TECH/product_database/images/910201410301AM_635_apple_iphone_6.jpeg"}} />
               </View>
           
               <View style={{flex: 1}}>

                   <Text style={{fontWeight:'bold',fontSize: 16,color:'black',marginLeft:12,marginTop:4}}>

                                 {rowData['item_name']}
                   </Text>

                   <Text style={{fontWeight:'bold',fontSize: 12,marginLeft:12,marginTop:4}}>
                            
                            {rowData['item_description']}
                   
                   </Text>               
              
              </View>
           
              <View style={{flex: 1,position:'absolute',right:8}}>
           
              <Icon2 name='pencil' size={20} color="#900" 
              />
           
              </View>                            

          </View>
      
        </TouchableHighlight>
      
      </View>    
    )
  }
  
_onFetchfix(pageNo, success, failure) {

}

async _onFetch(pageNo, success, failure) {

      console.log("Function Initial...");

    // userProfile = {"UserType":'dasda',"FirstName":'fname'};
    // userProfile2 = {"UserType":'dasda',"FirstName":'lname'};
    
   this.mylist = [];            
   console.log(this.iit +" count");


   // this.controller
                     
   console.log(this.str+" GET STRING");
   

   var getItems = await this.controller.getStoreItems(this.thisStoreId,this.str);
   console.log(JSON.stringify(getItems)+" \n\n\n\n\n\n\n\n "+JSON.stringify(this.mylist)+"\n\n\n\n\n\n\n\n");

   if(getItems.status)
    {
        if(getItems.data.length>0)
        {   
            var totalData = [];
            var setTotalBool = false;
            var getTotal = 0;
            for(var o in getItems.data) 
            {
              totalData[o] = getItems.data[o];
              getTotal = o;
            }

            this.mylist = totalData;
            success(this.mylist);
        }
        else
        {
          success([]);
        }
    }
    else
    {
      success([]);
    }

   
    // if(getItems.status)
    // {
    //     if(getItems.data.length>0)
    //     {     

              // var totalData = [];
              // var setTotalBool = false;
              // var getTotal = 0;
  //             for (var o in getItems.data) 
  //             {
  //               totalData[o] = getItems.data[o];
  //               getTotal = o;
  //             }

  //             if(this.total<getTotal)
  //             {
  //               this.total = getTotal;
  //               setTotalBool = true;
  //             }
          
  //            console.log("\n\n"+this.total+"\n\n"+getTotal);
  //            if(this.iit == 0)
  //            {   
  //             this.mylist = totalData;
  //             console.log(this.total);
  //            }else
  //             {
                
  //               if(getItems.data[this.total] && setTotalBool)
  //               {
  //                 this.mylist.push(getItems.data[this.total]);
  //               } 
  //               else
  //               { 
  //                 console.log("else 1");
  //               }
  //             }

  //             success(this.mylist);
  //             this.iit++
  //       }
  //       else
  //       {
  //         console.log("else 2");
  //         success([]);
  //       }

  //   }
  //   else
  //   {
  //     console.log("else 3");
  //     success([]);
  //   }
  }
}

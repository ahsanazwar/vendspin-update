import React, { Component, PropTypes } from 'react'
import {
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  InteractionManager,
  Navigator,
  StatusBar
} from 'react-native';

import {TabNavigator} from 'react-navigation';
import EasyListView from 'react-native-easy-listview-gridview';

import Icon2 from 'react-native-vector-icons/FontAwesome';
import {Thumbnail,Drawer,Icon,Button,List,ListItem} from 'native-base';

import MyStore from './buyerstore';
import MyCart from './mycart';
import MyOrder from './buyerorder';
import Settings from './settings';
const DATA_SIZE_PER_PAGE = 10
var data1,data2,data3;
import Controller from './controller/controller';



class BuyerStore extends Component {

  mylist=[];
  iit=0;
  static navigationOptions = {
     header: {visible:false},
     tabBar: {
            label: '',
            icon: ({ tintColor }) => {
                return (
                  <View>
                    <Icon2
                        name='tag'
                        size={27}
                        color={ "#fff" }
                    />
                  </View>  
                );

            }
        }
  };
  static propTypes = {
    empty: PropTypes.bool,
    error: PropTypes.bool,
    noMore: PropTypes.bool,
    column: PropTypes.number
  };

  static defaultProps = {
    empty: false,
    error: false,
    noMore: false,
    column: 1
  }
    
  constructor(props) {
    super(props)

    console.log("working");
    this.controller = new Controller();

    this.renderListItem = this._renderListItem.bind(this)
    this.onFetch = this._onFetch.bind(this)
    this.state = {
          renderPlaceholderOnly: true,
          toggled: false,
          store: {},
          theme: null
        };
  }


  async componentDidMount() {
      this.str = await this.controller.getUserStr();
      console.log(this.str +" STRING");
      this.radius = {
        radius:2
       }
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }



  toggleDrawer() 
  {
    this.state.toggled ? this._drawer.close() : this._drawer.open()
  }
  
  openDrawer() {
    this.setState({toggled: true})
  }
  closeDrawer() {
    this.setState({toggled: false})
  }
   

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
    <View style={{position:'relative',flex: 1,}}>  
      <Drawer
        ref={(ref) => this._drawer = ref}
        type="displace"
        content={<View style={{flex: 1,backgroundColor: "#fff", height: 1000}}>
                  <View style={{marginTop:5}}>
                     <Text style={{textAlign:'center',fontWeight:'bold',fontSize:20,color:'#6699ff'}}>Vendspin</Text>
                  </View>

                  <View>   
                      <List>
                        <ListItem last>
                            <Text style={{color:'#6699ff'}}>My Profile</Text>
                        </ListItem>

                        <ListItem last>
                            <Text style={{color:'#6699ff'}}>Settings</Text>
                        </ListItem>

                        <ListItem last>
                            <Text style={{color:'#6699ff'}}>Help</Text>
                        </ListItem>

                        <ListItem>
                            <Text style={{color:'#6699ff'}}>Feedback</Text>
                        </ListItem>        
                      </List>
                  </View>

                  <View>   
                      <List>
                        <ListItem last>
                            <Text style={{color:'#6699ff'}}>My Profile</Text>
                        </ListItem>

                        <ListItem last>
                            <Text style={{color:'#6699ff'}}>Settings</Text>
                        </ListItem>

                        <ListItem last>
                            <Text style={{color:'#6699ff'}}>Help</Text>
                        </ListItem>

                        <ListItem>
                            <Text style={{color:'#6699ff'}}>Feedback</Text>
                        </ListItem>        
                      </List>
                  </View>

                  <View>   
                      <List>
                        <ListItem last>
                            <Text
                            onPress= {() => navigate('root')}
                             style={{color:'#000000'}}>Logout</Text>
                        </ListItem>        
                      </List>
                  </View>          
                 
                 </View>}
        onClose={this.closeDrawer.bind(this)}
        onOpen={this.openDrawer.bind(this)}
        openDrawerOffset={0.2}
        side="left"
        panOpenMask={.25}
        >
        <EasyListView 
        ref={component => this.listview = component}
        dataSizePerPage={DATA_SIZE_PER_PAGE}
        column={this.props.column}
        rowHeight={60}
        renderItem={this.renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetchfix}
        style={{marginTop:20}}
      />
        </Drawer>

        <StatusBar
          backgroundColor="#7a013e"
         barStyle="light-content"/>
    </View>  
    )
  }

  _renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }

  _renderListItem(rowData, sectionID, rowID, highlightRow) {
    const { navigate } = this.props.navigation;

    return (
      
      <View style={{position:'relative',flex: 1,padding:4,flexDirection:'row'}}>

        <TouchableHighlight
          style={{flex: 1}}
          onPress= {() => navigate('viewstore', {store_name:rowData.store_name,store_id:rowData.store_id})}
          underlayColor="#fff">
          <View style={{flex: 1,borderBottomWidth:1, flexDirection: 'row'}}>
           
              <View>
           
               <Thumbnail style={{width: 80, height: 80, }} square 
               source={{uri:"http://drop.ndtv.com/TECH/product_database/images/910201410301AM_635_apple_iphone_6.jpeg"}} />
               </View>
           
               <View style={{flex: 1}}>

                   <Text style={{fontWeight:'bold',fontSize: 16,color:'black',marginLeft:12,marginTop:4}}>
                           {rowData['store_name']}
                   </Text>

                   <Text style={{fontWeight:'bold',fontSize: 12,marginLeft:12,marginTop:4}}>
                            
                            Description
                   
                   </Text>               
              
              </View>

              <View style={{flex: 1,position:'absolute',right:8,top:4}}>
           
              <Text note style={{color:'#ff751a'}}>Qty:</Text>
           
              </View>
                            
           
          </View>
      
        </TouchableHighlight>

        <View />
      </View>
    )
  }
  
_onFetchfix(pageNo, success, failure) {

}

async _onFetch(pageNo, success, failure) {

      console.log("Function Initial...");

    // userProfile = {"UserType":'dasda',"FirstName":'fname'};
    // userProfile2 = {"UserType":'dasda',"FirstName":'lname'};
    
   this.mylist = [];            
   console.log(this.iit +" count");


   // this.controller
                     
   console.log(this.str+" GET STRING");
   

   var getItems = await this.controller.getAllStore(this.radius,this.str);
   console.log(JSON.stringify(getItems)+" \n\n\n\n\n\n\n\n "+JSON.stringify(this.mylist)+"\n\n\n\n\n\n\n\n");

   if(getItems)
   {
      if(getItems.status)
      {
          if(getItems.data.length>0)
          {   
              var totalData = [];
              var setTotalBool = false;
              var getTotal = 0;
              for(var o in getItems.data) 
              {
                totalData[o] = getItems.data[o];
                getTotal = o;
              }

              this.mylist = totalData;
              success(this.mylist);
          }
          else
          {
            success([]);
          }
      }
      else
      {
        success([]);
      }
  }
  else
  {
    success([]);
  }
   
    // if(getItems.status)
    // {
    //     if(getItems.data.length>0)
    //     {     

              // var totalData = [];
              // var setTotalBool = false;
              // var getTotal = 0;
  //             for (var o in getItems.data) 
  //             {
  //               totalData[o] = getItems.data[o];
  //               getTotal = o;
  //             }

  //             if(this.total<getTotal)
  //             {
  //               this.total = getTotal;
  //               setTotalBool = true;
  //             }
          
  //            console.log("\n\n"+this.total+"\n\n"+getTotal);
  //            if(this.iit == 0)
  //            {   
  //             this.mylist = totalData;
  //             console.log(this.total);
  //            }else
  //             {
                
  //               if(getItems.data[this.total] && setTotalBool)
  //               {
  //                 this.mylist.push(getItems.data[this.total]);
  //               } 
  //               else
  //               { 
  //                 console.log("else 1");
  //               }
  //             }

  //             success(this.mylist);
  //             this.iit++
  //       }
  //       else
  //       {
  //         console.log("else 2");
  //         success([]);
  //       }

  //   }
  //   else
  //   {
  //     console.log("else 3");
  //     success([]);
  //   }
  }
}




const buyerstore = TabNavigator({

  "My Store": {screen: BuyerStore},
  "my cart":  {screen: MyCart},
  "my order": {screen: MyOrder},
  "settings": {screen: Settings},

},
{
    tabBarOptions: {
    showLabel:false,
    showIcon: true,
    iconStyle: {
        width: 35,
        height: 30
    },
    pressColor: '#d5dcea',
    style:{backgroundColor:"#99004d"},
    indicatorStyle: {backgroundColor: '#ffb3ff'},
    labelStyle:{fontSize: 11},  
    }
},
);

export default buyerstore;

import React from 'react';

import {TabNavigator} from 'react-navigation';

import {Button} from 'native-base';
import {Image,Text} from 'react-native';

export default class Setup extends React.Component {
  static navigationOptions = {
    label: 'Home',
    header: {visible:false}
  };

  render() {
    const { goBack } = this.props.navigation;
    return (
      <Button
        title="Go back to home tab"
        onPress={() => goBack()}
      >
      <Text>Hello Setup</Text>
      </Button>
    );
  }
}


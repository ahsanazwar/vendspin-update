import React, { Component, PropTypes } from 'react'
import {
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  InteractionManager
} from 'react-native';

import EasyListView from 'react-native-easy-listview-gridview';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialIcons'
import {Thumbnail} from 'native-base';

const DATA_SIZE_PER_PAGE = 10
var data1,data2,data3;

export default class MyOrder extends Component {
  mylist=[];
  iit=0;
  static navigationOptions = {
     header: {visible:false},
     tabBar: {
            label: '',
            icon: ({ tintColor }) => {
                return (
                  <View>
                    <Icon3
                        name='account-balance'
                        size={27}
                        color={ "#fff" }
                    />
                  </View>  
                );
              }
            }
  };
  static propTypes = {
    empty: PropTypes.bool,
    error: PropTypes.bool,
    noMore: PropTypes.bool,
    column: PropTypes.number
  };

  static defaultProps = {
    empty: false,
    error: false,
    noMore: false,
    column: 1
  }
      measureView(event){      
            this.setState({
            x: event.nativeEvent.layout.x,
            y: event.nativeEvent.layout.y,
            width: event.nativeEvent.layout.width,
            height: event.nativeEvent.layout.height
                     })
       }
  constructor(props) {
    super(props)

    this.renderListItem = this._renderListItem.bind(this)
    this.onFetch = this._onFetch.bind(this)
    this.state = {
          renderPlaceholderOnly: true
        };
  }

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
      <EasyListView 
        ref={component => this.listview = component}
        dataSizePerPage={DATA_SIZE_PER_PAGE}
        column={this.props.column}
        rowHeight={60}
        renderItem={this.renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetchfix}
        style={{marginTop:20}}
      />
    )
  }

  _renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }

  _renderListItem(rowData, sectionID, rowID, highlightRow) {
    const { navigate } = this.props.navigation;
    return (
      <View style={{position:'relative',flex: 1,padding:4,flexDirection:'row'}}>

        <TouchableHighlight
          style={{flex: 1}}
          onPress= {() => navigate('viewOrder', {order:rowData['Order']})}
          underlayColor="#fff">
          <View style={{flex: 1,borderBottomWidth:1, flexDirection: 'row',backgroundColor: (rowData['Key'] === true) ? '#990000' : '#00ff00'}}>
           
              <View>
           
               <Thumbnail style={{width: 80, height: 80, }} square 
               source={{uri:"http://drop.ndtv.com/TECH/product_database/images/910201410301AM_635_apple_iphone_6.jpeg"}} />
               </View>
           
               <View style={{flex: 1}}>

                   <Text style={{color: (rowData['Key'] === true) ? '#fff' : '#000000',fontWeight:'bold',fontSize: 16,marginLeft:12,marginTop:4}}>

                           {rowData['Order']}
                   </Text>

                   <Text style={{color: (rowData['Key'] === true) ? '#fff' : '#000000',fontWeight:'bold',fontSize: 12,marginLeft:12,marginTop:4}}>
                            
                            {rowData['Description']}
                   
                   </Text>               
              
              </View>
           
          </View>
      
        </TouchableHighlight>

        <View />
      </View>
    )
  }
  
_onFetchfix(pageNo, success, failure) {

}
  _onFetch(pageNo, success, failure) {

      console.log("Function Initial...");

    // userProfile = {"UserType":'dasda',"FirstName":'fname'};
    // userProfile2 = {"UserType":'dasda',"FirstName":'lname'};

    data1 = {"Order":'Order a',"Description":'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',"Key":true};
    data2 = {"Order":'Order b',"Description":'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',"Key":false};
    data3 = {"Order":'Order c',"Description":'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',"Key":true};
             
   console.log(this.iit);
   if(this.iit == 0)
  {   
 //   this.mylist=[userProfile,userProfile2];
      this.mylist=[data1,data2,data3];
    //success([]); 
    success(this.mylist);
  }else
    {
     // this.mylist.push(userProfile);
      this.mylist.push(data1);  
      success(this.mylist); 
     // success(this.mylist);
    }

    this.iit++   
  }
}

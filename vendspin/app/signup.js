import React from 'react';

import  {
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Alert,
  InteractionManager
} from 'react-native';
import { View, Container, Content, Form, Item,Thumbnail,Badge,
       Input,StyleProvider, Text, Button,Header,Body,Right,Left,Icon,
       Title,List,ListItem,ActionSheet } from 'native-base';

import { RadioGroup } from 'nachos-ui'
import Prompt from 'react-native-prompt';

import ImagePicker from 'react-native-image-crop-picker';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import HideableView from 'react-native-hideable-view';

import { StackNavigator } from 'react-navigation';

import Dashboardseller from './dashboardseller';
import Dashboardbuyer from './dashboardbuyer';
import Controller from './controller/controller';


var BUTTONS = [
  'Camera',
  'Gallery',
  'Cancel',
];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;


export default class Signup extends React.Component {
        constructor(props) {
        super(props);
        this.controller = new Controller();
        this.state = {
              visible: false,
              value: 'Buyer',
              userValue: null,
              fName:'',
              lName:'',
              eMail:'',
              password:'',
              confirmPass:'',
              storeName:'',
              renderPlaceholderOnly: true,   
              fieldImage:"",
              storeName:'', 
              loader:false,
        };
       
    } 

    isEmptyField()
    {
          if (this.state.fName=="" || this.state.lName=="" 
              || this.state.eMail=="" || this.state.password=="" 
              || this.state.fName=="" || this.state.fName=="" ) 
            {
              return true;
            } 
            else
            {
              if (this.state.value == "Seller") 
                {
                  if (this.state.storeName=="") 
                    {
                      return true;
                    }
                    else
                    {
                      return false;
                    }
                }
                else 
                {
                   return false;
                }
            }
        }
    
    matchPassword()
    {
      if (this.state.password == this.state.confirmPass) 
        {
            return true;
        }
         else 
         {
            return false;
         }
    }

    async submitProfile()
    { 
       if (this.isEmptyField() == true) 
       {
          Alert.alert("Please Fill All The Fields!");
       }
       else
       {
          if (this.matchPassword() == true)
           {
              var userProfile = {};
              userProfile = {
              type:this.state.value,
              fname:this.state.fName, 
              lname:this.state.lName,
              email:this.state.eMail,
              password:this.state.password,
              img:this.state.fieldImage,
              store_name:this.state.storeName};
                
              console.log("To register "+userProfile.email)
              
              var get = await this.controller.setProfile(userProfile);
              if(get)
              {
                console.log(get.status +" Signup confirmation");

                if(get.status == false)
                {
                  Alert.alert(get.msg);
                }
                else
                {   
                    if(get.data.user_type == "Seller")
                    {
                      this.props.navigation.navigate('selling',{User:get.data.user_type})
                    }
                    else if(get.data.user_type == "Buyer")
                    {
                     this.props.navigation.navigate('buyerstore',{User:get.data.user_type})
                    }
                }
              }
              else
              {
                
                Alert.alert("Sorry something went wrong.");
              }
           } 
           else 
           {
              Alert.alert("Your Passwords Does Not Match Please Type Again!");
           }
       }
            
    }

    pickSingleWithCamera(cropping) 
    {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
      includeBase64:true
    }).then(image => {
      
      this.setState({
        image: {uri: image.path, width: image.width, height: image.height},
        images: null
      });
      this.state.fieldImage = image.data;


    }).catch(e => alert(e));
  }

  pickSingle(cropit, circular=false) {
    ImagePicker.openPicker({
      width: 70,
      height: 70,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      compressVideoPreset: 'MediumQuality',
      includeBase64:true
    }).then(image => {
      

      this.setState({
        image: {uri: image.path , width: image.width, height: image.height, mime: image.mime},   
        images: null
      });
      this.state.fieldImage = image.data;

    }).catch(e => {
      
      Alert.alert(e.message ? e.message : e);
    });
  }

  scaledHeight(oldW, oldH, newW) 
  {
    return (oldH / oldW) * newW;
  }
  
  renderImage(image) 
  {
  return <Thumbnail style={{width: 105, height: 105,borderRadius: 90,
          borderWidth: 0.3,borderColor: '#cc0066'}} source={image} />
  }

  renderAsset(image) 
  {
    if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
      return this.renderVideo(image);
    }

    return this.renderImage(image);
  }

  validate(check)
  {
     console.log(check);
  }

    static navigationOptions = {
    header: 
    {
      visible:true,

      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    },
    title: "Sign up"
  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }
     
    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

  // return (

  //   if(this.state.loader == true)
  //   {
  //     return null;
  //   }
  //   else
  //   {
    return (
    <Container >

        <Content>
          
      <Container style={{flex:1,marginTop:8}}>
              <Form>
      <Button style={{marginBottom:8,alignSelf:'center',height:105}} transparent
             onPress={()=> ActionSheet.show(
                  {
                    options: BUTTONS,
                    cancelButtonIndex: CANCEL_INDEX,
                    destructiveButtonIndex: DESTRUCTIVE_INDEX,
                    title: 'Profile Pic',
                  },
                  (buttonIndex) => {            
                    if (buttonIndex==0) 
                      {
                        this.pickSingleWithCamera(false)
                          
                      } 
                     else
                     if (buttonIndex==1) 
                     {
                        this.pickSingle(false)
                     } 
                  }
                  )}>
        
        {this.state.image ? this.renderAsset(this.state.image) : 
          <Thumbnail style={{width: 105, height: 105,borderRadius: 90,
            borderWidth: 0.3,borderColor: '#cc0066'}} source={require('./img/defaultProfile.jpg')} /> 
        }

        {this.state.images ? this.state.images.map(i => 
        <View key={i.uri}>{this.renderAsset(i)}</View>) : 
        null}

        <Badge style={{position:'absolute',bottom:0,right:18,backgroundColor:'#99004d'}}>
        <Icon2 name='camera' size={17} color="white" style={{flex: 1,justifyContent: 'center',marginTop:5}}/></Badge> 
      </Button>  
                  
                  <RadioGroup
                  onChange={value => this.setState({ value  })}
                  defaultSelected={'Buyer'}
                  options={['Buyer','Seller']}
                  style={[{marginLeft:20,marginBottom:8}]}
                />
                  

                  <Item last>
                        <Input 
                          placeholder='First Name'
                          returnKeyLabel = {"next"}
                          onChangeText={(text) => this.setState({fName: text})} />
                    </Item>
                  <Item last>
                      <Input 
                        placeholder="Last Name"
                        returnKeyLabel = {"next"}
                        onChangeText={(text) => this.setState({lName: text})} />
                  </Item>

                  <Item last>
                      <Input 
                        placeholder="Email"
                        returnKeyLabel = {"next"}
                        onChangeText={(text) => this.setState({eMail: text})} 
                      />
                  </Item>

                  <Item last>
                      <Input placeholder="Password" 
                      secureTextEntry={true}
                      returnKeyLabel = {"next"}
                      onChangeText={(text) => this.setState({password: text})} 
                      />
                  </Item>

                  <Item last>
                      <Input placeholder="Confirm Password" secureTextEntry={true}
                      //  returnKeyLabel = {{this.state.value=="Seller" ? "next" : "done"}}
                        onChangeText={(text) => this.setState({confirmPass: text})} 
                      />
                  </Item>

                  <HideableView style={{marginBottom:35}} visible={this.state.value=="Seller" ? true : false}>
                      <Item last>
                        <Input placeholder="Store Name" 
                          returnKeyLabel = {"done"}
                          onChangeText={(text) => this.setState({storeName: text})} 
                      />
                    </Item>
                  </HideableView>


                <Container style={{flex:1,justifyContent: 'center',}}>
                  <Button   
                       onPress={() => this.submitProfile()}  
                       style={{alignSelf: 'center',width:170,
                            backgroundColor:'#808080'}} >
                        <Text style={{flex: 1,textAlign:'center',fontSize:15}}>Signup</Text>
                  </Button>
                </Container>  
              </Form>
              </Container>
              </Content>
        </Container>  
)
  }

_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};

import React from 'react';
import {
          View,
          InteractionManager,
          ScrollView,
          Text
       } from 'react-native';

// import { Container, Content, Form, Item, Badge,
//        Input, Text, Button,StyleProvider,Header,Body,Right,Left,Icon,
//        Title,List,ListItem,Thumbnail,ActionSheet } from 'native-base';

export default class Sponsor extends React.Component {

  constructor(props) {

        super(props);
        this.state = {

          renderPlaceholderOnly: true
        };
      }


/////////////////////////////////////PlaceHolder////////////////////////////////////
    
  static navigationOptions = {
    title:"Sponsors",
    header: {visible:true},

  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
       <View>
        <Text>Sponsor</Text>
       </View>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};


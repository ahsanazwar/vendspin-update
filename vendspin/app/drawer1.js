import React,{Component} from 'react';
import {View,Text} from 'react-native';
import {Drawer} from 'native-base';
class Drawer1 extends Component {
    render() {
        closeDrawer = () => {
        this.drawer.root.close()
      };
      openDrawer = () => {
        this.drawer.root.open()
      };
        return (
            <Drawer
              ref={(ref) => { this.drawer = ref; }}
              content={<View style={{backgroundColor:"black",height:1000}} />}
              onClose={() => this.closeDrawer()} >
          </Drawer>
        );
    }
}

export default Drawer1;
import React, { Component, PropTypes } from 'react'
import {
  Text,
  View,
  TouchableHighlight,
  Dimensions,
  InteractionManager,
  ScrollView
} from 'react-native';

import EasyListView from 'react-native-easy-listview-gridview';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import {Thumbnail,Button} from 'native-base';

const DATA_SIZE_PER_PAGE = 10
var data1,data2,data3;

export default class CartItems extends Component {
  mylist=[];
  iit=0;
  static navigationOptions = {
     header: 
    {
      visible:true,

      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    },
     title: ({state}) => `${state.params.store}`
  };
  static propTypes = {
    empty: PropTypes.bool,
    error: PropTypes.bool,
    noMore: PropTypes.bool,
    column: PropTypes.number
  };

  static defaultProps = {
    empty: false,
    error: false,
    noMore: false,
    column: 1
  }
    
  constructor(props) {
    super(props)

    this.renderListItem = this._renderListItem.bind(this)
    this.onFetch = this._onFetch.bind(this)
    this.state = {
          renderPlaceholderOnly: true
        };
  }

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }
    return (
     <View style={{position:'relative',flex: 1}}> 
      <EasyListView 
        ref={component => this.listview = component}
        dataSizePerPage={DATA_SIZE_PER_PAGE}
        column={this.props.column}
        rowHeight={60}
        renderItem={this.renderListItem}
        refreshHandler={this.onFetch}
        loadMoreHandler={this.onFetchfix}
        style={{marginTop:20}}
        containerHeight = {500}
      />
      <View style={{flex: 1,position:'absolute',bottom:10,left:8}}>
            <Text style={{color:'black',fontWeight:'bold'}}>Total Items 10 </Text>
            <Text style={{color:'black',fontWeight:'bold'}}>Total Price 2000$</Text>
      </View>
      
      <View style={{flex: 1,position:'absolute',right:8,bottom:10}}>
            <Button style={{height:30}}><Text style={{color:'#fff'}}>Order Now</Text></Button>
      </View>
     </View> 
    );
  }

  _renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }

  _renderListItem(rowData, sectionID, rowID, highlightRow) {
    const { navigate } = this.props.navigation;
    return (
      <View style={{position:'relative',flex: 1,padding:4,flexDirection:'row'}}>

        <TouchableHighlight
          style={{flex: 1}}
          onPress= {() => navigate('editItems',{item:rowData['Items']})}
          underlayColor="#fff">
          <View style={{flex: 1,borderBottomWidth:1, flexDirection: 'row'}}>
           
              <View>
           
               <Thumbnail style={{width: 80, height: 80, }} square 
               source={{uri:"http://drop.ndtv.com/TECH/product_database/images/910201410301AM_635_apple_iphone_6.jpeg"}} />
               </View>
           
               <View style={{flex: 1}}>

                   <Text style={{fontWeight:'bold',fontSize: 16,color:'black',marginLeft:12,marginTop:4}}>

                                 {rowData['Items']}
                   </Text>

                   <Text style={{fontWeight:'bold',fontSize: 12,marginLeft:12,marginTop:4}}>
                            
                            {rowData['Description']}
                   
                   </Text>               
              
              </View>

              <View style={{flex: 1,position:'absolute',right:8,top:4}}>
           
                <Icon2 name='close' size={20} color="#900"/>
           
              </View>                            

          </View>
      
        </TouchableHighlight>

        <View />
      </View>
    )
  }
  
_onFetchfix(pageNo, success, failure) {

}
  _onFetch(pageNo, success, failure) {

      console.log("Function Initial...");

    // userProfile = {"UserType":'dasda',"FirstName":'fname'};
    // userProfile2 = {"UserType":'dasda',"FirstName":'lname'};

    data1 = {"Items":'Item a',"Description":'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'};
    data2 = {"Items":'Item b',"Description":'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'};
    data3 = {"Items":'Item c',"Description":'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'};
             
   console.log(this.iit);
   if(this.iit == 0)
  {   
 //   this.mylist=[userProfile,userProfile2];
      this.mylist=[data1,data2,data3];
    //success([]); 
    success(this.mylist);
  }else
    {
     // this.mylist.push(userProfile);
      this.mylist.push(data1);  
      success(this.mylist); 
     // success(this.mylist);
    }

    this.iit++
    
  }
}

// start FROM HERE
import React from 'react';
import {
          View,
          InteractionManager,
          ScrollView,
          Alert
       } from 'react-native';

import { Container, Content, Form, Item, Badge,
       Input, Text, Button,StyleProvider,Header,Body,Right,Left,Icon,
       Title,List,ListItem,Thumbnail,ActionSheet } from 'native-base';

import Icon2 from 'react-native-vector-icons/FontAwesome';


import Controller from './controller/controller';

var BUTTONS = [
  'Camera',
  'Gallery',
  'Cancel',
];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

export default class SellerProfile extends React.Component {


  constructor(props) {
        super(props);
        this.controller = new Controller();
        this.state = 
        {
          fName:'',
          lName:'',
          storeName:'',
          password:'',
          renderPlaceholderOnly: true,
          userData:
          {
            init:0,
            data:
            {
              user_fname:"---",
              user_lname:"---",
              store_name:"---",
            }
          },
          user_pic:''
        };
        
  }

  isEmptyField()
    {
           if (this.state.fName=="" && this.state.lName==""
              && this.state.storeName=="" && this.state.password=="")
              {
                  return true;
              }
              else
              {
                  return false;
              } 
    }
      
  async onSubmit()
  {
          if (this.isEmptyField()==true) 
            {
                  Alert.alert("Please fill any field for updation");
            } 
            else 
            {     

                  var userProfile = {};
                  userProfile = {
                  fname:this.state.fName, 
                  lname:this.state.lName,
                  password:this.state.password,
                  store_name:this.state.storeName};

                  var str = await this.controller.getUserStr();
                  
                  var getData = await this.controller.updateUser(userProfile,str);
                  if(getData)
                  {
                    Alert.alert(getData.msg); 
                  }
                  else
                  {
                    Alert.alert("Some thing went wrong");
                  }
                  

            }
  }




    pickSingleWithCamera(cropping) 
    {
    ImagePicker.openCamera({
      cropping: cropping,
      width: 500,
      height: 500,
    }).then(image => {
      
      this.setState({
        image: {uri: image.path, width: image.width, height: image.height},
        images: null
      });
    }).catch(e => alert(e));
  }

  pickSingle(cropit, circular=false) {
    ImagePicker.openPicker({
      width: 70,
      height: 70,
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 640,
      compressImageMaxHeight: 480,
      compressImageQuality: 0.5,
      compressVideoPreset: 'MediumQuality',
    }).then(image => {
      
      this.setState({
        image: {uri: image.path, width: image.width, height: image.height, mime: image.mime},
        images: null
      });
    }).catch(e => {
      
      Alert.alert(e.message ? e.message : e);
    });
  }

  scaledHeight(oldW, oldH, newW) {
    return (oldH / oldW) * newW;
  }
  
   renderImage(image) {
    return <Thumbnail style={{width: 105, height: 105,borderRadius: 90,
            borderWidth: 0.3,borderColor: '#cc0066'}} source={image} />
  }

  renderAsset(image) {
    if (image.mime && image.mime.toLowerCase().indexOf('video/') !== -1) {
      return this.renderVideo(image);
    }

    return this.renderImage(image);
  }

/////////////////////////////////////PlaceHolder////////////////////////////////////
    
  static navigationOptions = {
    header: {visible:false},
    tabBar: {
            label: '',
            icon: ({ tintColor }) => {
                return (
                  <View>
                    <Icon2
                        name='user'
                        size={27}
                        color={ "#fff" }
                    />
                  </View>  
                );

            }
        }
  };

  componentDidMount() {
      this.userProfile();
  };
   
  async componentWillMount() 
  {
    
  }

    
    async userProfile()
    {
        var getAuth = await this.controller.auth();
        var str = await this.controller.getUserStr();
          
          
          var getInstance = await this.controller.getUser(str);
          console.log(JSON.stringify(getInstance)+" GET PIC")
          if(getInstance)
          {
            
            InteractionManager.runAfterInteractions(() => {
              this.setState({userData: getInstance});
              this.setState({renderPlaceholderOnly: false}); 
            });
            
          }
          else
          {   
              console.log("NON SERVICE");
              this.userLocalProfile();
          }
        
    };
    

    async userLocalProfile()
    {
        var getLocalData  = await this.controller.getLocalProfile();
        this.state.userData = getLocalData;
    }

    render() {
      
      const { navigate } = this.props.navigation;

    
    if (this.state.renderPlaceholderOnly) 
    { 
      return this._renderPlaceholderView();
    }
    else
    {
    return (
    <View style={{position:'relative',flex: 1,}}>
          <View>
            
          </View>
        <Container style={{flex:1,justifyContent:'center'}}>
          <Form>

                  <Button style={{marginBottom:8,alignSelf:'center',height:105}} transparent
                           onPress={()=> ActionSheet.show(
                  {
                    options: BUTTONS,
                    cancelButtonIndex: CANCEL_INDEX,
                    destructiveButtonIndex: DESTRUCTIVE_INDEX,
                    title: 'Profile Pic',
                  },
                  (buttonIndex) => {            
                    if (buttonIndex==0) 
                      {
                        this.pickSingleWithCamera(false)
                          
                      } 
                     else
                     if (buttonIndex==1) 
                     {
                        this.pickSingle(false)
                     } 
                  }
                  )}>
        
        {this.state.image ? this.renderAsset(this.state.image) : 
          <Thumbnail style={{width: 105, height: 105,borderRadius: 90,
            borderWidth: 0.3,borderColor: '#cc0066'}} source={require('./img/defaultProfile.jpg')} /> 
        }

        {this.state.images ? this.state.images.map(i => 
        <View key={i.uri}>{this.renderAsset(i)}</View>) : 
        null}

        <Badge style={{position:'absolute',bottom:0,right:18,backgroundColor:'#99004d'}}>
        <Icon2 name='camera' size={17} color="white" style={{flex: 1,justifyContent: 'center',marginTop:5}}/></Badge> 
      </Button>


              <Item last>
                    <Input value={this.state.userData.data.user_fname}
                    onChangeText={(text) => this.setState({fName: text})}/>
                </Item>
              <Item last>
                  <Input value={this.state.userData.data.user_lname}
                  onChangeText={(text) => this.setState({lName: text})}/>
              </Item>

              <Item last>
                  <Input value={this.state.userData.data.store_name}
                  onChangeText={(text) => this.setState({storeName: text})}/>
              </Item>

              <Item last>
                  <Input placeholder="Change Password"
                  onChangeText={(text) => this.setState({password: text})}/>
              </Item>
              <Button   onPress={() => this.onSubmit()}
                        style={{marginTop:30,alignSelf: 'center',width:170,
                              backgroundColor:'#808080'}} >
                    <Text style={{marginLeft:45,fontSize:15}}>Update</Text>
              </Button>
        
          </Form>
        </Container> 
      </View>
  );
    }

    
    
  }
_renderPlaceholderView() {

    return (
      <View>

      </View>

    );
  }
};


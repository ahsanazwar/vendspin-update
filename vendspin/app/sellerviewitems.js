
import React from 'react';
import {
          View,Text,InteractionManager,ScrollView,
          Image,StyleSheet,
       } from 'react-native';

import { ListItem,List,Thumbnail,Container,Content,Left,Right,Body,CardItem
         ,Button,Card,Item,Input,Header,Title,Icon,StyleProvider } from 'native-base';

import Icon2 from 'react-native-vector-icons/FontAwesome';       
import LocalImage from './LocalImage';
import Controller from './controller/controller';

export default class SellerViewItems extends React.Component {

  constructor(props) {

        super(props);
        this.controller = new Controller();
        this.state = {

                title:'',
                description:'',
                price:'',
                quantity:'',

          renderPlaceholderOnly: true
        };
      }


           isEmptyField()
          {
             if (this.state.title=="" && this.state.description==""
                && this.state.price=="" && this.state.quantity=="")
                {
                    return true;
                }
                else
                {
                    return false;
                }
          }
        onSubmit()
      {
          if (this.isEmptyField()==true) 
            {
                  return "Please fill any field for updation";
            } 
            else 
            {
                var userProfile = {};
                 userProfile = {"Title":this.state.title,
                 "Description":this.state.description,"Price":this.state.price,
                 "Quantity":this.state.quantity};
                 return "Updated";
            }
      }


  ////////////////////////////////////////////PlaceHolder//////////////////////////////////////////////    


  static navigationOptions = {
    header: 
    {
      visible:true,

      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    },
    title: ({state}) => `${state.params.item}`
  };

  async componentDidMount() {
      this.str = await this.controller.getUserStr();

      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
    <Container style={{backgroundColor:'white'}}>
              <Content>

                        <List>        
                    <Card >
                          <CardItem cardBody>
                              <LocalImage 
                                source={require('./img/Items/camera.jpg')}
                                orignalWidth = {600}
                                orignalHeight = {489}
                                />
                          </CardItem>
                   </Card>

                    <ListItem last>
                        <Body>

                              <Item last>
                                  <Input placeholder="Title"
                                  onChangeText={(text) => this.setState({Title: text})}/>
                              </Item>

                              <Item last>
                                  <Input placeholder="Description"
                                  onChangeText={(text) => this.setState({Description: text})}/>
                              </Item>

                              <Item last>
                                  <Input keyboardType = 'numeric' placeholder="Price"
                                  onChangeText={(text) => this.setState({Price: text})}/>
                              </Item>

                              <Item last>
                                  <Input keyboardType = 'numeric' placeholder="Quantity"
                                  onChangeText={(text) => this.setState({Quantity: text})}/>
                              </Item>
                        </Body>
                    </ListItem>
                    <ListItem>
                    <Body>
                    <Button style={{width:170,backgroundColor:'#99004d',
                         alignSelf: 'center',flex: 1,}}
                         onPress={() => alert(this.onSubmit())}
                    >
                        <Text style={{textAlign:'center',flex:1,fontWeight:'bold',color:'white'}} >Submit</Text>
                  </Button>
                  </Body>
                  </ListItem>

                </List>

                    </Content>
                </Container>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};


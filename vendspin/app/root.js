import React from 'react';
import {
  AppRegistry,
  Text,
  StyleSheet,
  Image,
  View,
  TouchableOpacity,
  StatusBar,
  InteractionManager,
  AppState,
  Platform
} from 'react-native';
import { Thumbnail} from 'native-base';
import { StackNavigator,TabNavigator } from 'react-navigation';

///////////////////Push Notification/////////////////////////
import PushController from './pushController';
import PushNotification from 'react-native-push-notification';

import Login from './login';
import BuyerProfile from './buyerprofile';
import Signup from './signup';
import Selling from './selling';
import MyStore from './mystore';
import SellerViewStore from './sellerviewstore';
import SellerViewItems from './sellerviewitems';
import EditItems from './editItems';
import ViewOrder from './viewOrder';
import BuyerStore from './buyerstore';
import ViewStore from './viewstore';
import ViewItems from './viewitems';
import CartItems from './cartitems';
import AddItems from './additems';
import SellerEditItems from './selleredititems';
import BuyerOrderView from './buyerorderview';
import Sponsor from './sponsor'

import Controller from './controller/controller';
const timer = require('react-native-timer');

class Root extends React.Component {
  
  static navigationOptions = {
    title: 'Welcome',
    header: {visible:false},
  };


  constructor(props) 
  { 

      super(props);
      this.handleAppStateChange = this.handleAppStateChange.bind(this);
      this.controller = new Controller(); 
      this.state = {
        user:true,
      };
  }; 

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange(appState) {
    if (appState === 'background') {
      let date = new Date(Date.now() + (5 * 1000));

      if (Platform.OS === 'ios') {
        date = date.toISOString();
      }

      PushNotification.localNotificationSchedule({
        message: "My Notification Message",
        date,
      });
    }
  }

  async componentDidMount() 
  {
     AppState.addEventListener('change', this.handleAppStateChange); 
     var userData = await this.controller.auth();
     console.log(userData+" getting start data")

     if(userData != false)
      { 
        if(userData.user_type == "Seller")
        {   

          this.props.navigation.navigate('selling',{User:userData.user_type})
        }
        else if(userData.user_type == "Buyer")
        {
          this.props.navigation.navigate('buyerstore',{User:userData.user_type})  
        }
      }
      else
      {
        console.log("clear All fucking shit")

        InteractionManager.runAfterInteractions(() => {
          this.setState({user : false});
        });
        
        this.controller.clearData();
      }
  };


  render() 
  {
    const { navigate } = this.props.navigation;
    console.log(this.state.user+" user id");
    if(this.state.user == true)
    {
      return null;
    }
    else
    {
    return (
    <Image source={require('./img/bg1.png')}
         style={styles.containerBackground}> 

               <Thumbnail style={{width: 100,height: 100}}
                square source={require('./img/logo.jpg')} />
        
        <Text style={styles.txt1}>Vendspin</Text>
        <View style={styles.bottomView}>            
          <Text style={styles.txt2}>Use App I am a</Text>
           <View style={styles.btnContainer}>   
              <TouchableOpacity style={styles.buyerBtn} 
              onPress={() => navigate('login')}
              >
                  <Text style={styles.btnText1}>Login</Text>
              </TouchableOpacity>

              <TouchableOpacity style={styles.sellerBtn} 
              onPress={() => navigate('signup')}
              >
              <Text style={styles.btnText2}>Signup</Text>
              </TouchableOpacity>
            </View>  
            </View>
        <PushController />  
        <StatusBar
          backgroundColor="#7a013e"
         barStyle="light-content"/>
  </Image>
  );
    }
  }
}

const vendspin = StackNavigator({
  root: { screen: Root },
  signup: { screen: Signup },
  login: { screen: Login },
  selling: {screen: Selling},
  sellerviewstore: {screen: SellerViewStore},
  sellerviewitems: {screen: SellerViewItems},
  editItems: {screen: EditItems},
  viewOrder: {screen: ViewOrder},
  buyerstore: {screen: BuyerStore},
  viewstore: {screen: ViewStore},
  viewitems: {screen: ViewItems},
  cartitems: {screen: CartItems},
  additems: {screen: AddItems},
  selleredititems: {screen: SellerEditItems},
  buyerorderview: {screen: BuyerOrderView},
  buyerprofile: {screen: BuyerProfile},
  sponsor: {screen: Sponsor},

},
{
    headerMode: 'screen',
    headerStyle:{backgroundColor:"blue"},
    headerTintColor:'blue',
    container:{backgroundColor:'blue'},
}
);


const styles = StyleSheet.create({
  containerBackground: {
    flex: 1,
    justifyContent: 'center',
    alignItems:'center',
    width:null,
    height:null,
    position:'relative',
//    resizeMode:'stretch',
  },
  
  txt1:{
    fontSize: 28,
    fontWeight:'bold',
    color:'#ffff',
    marginLeft:3
  },

 bottomView:{
  flex: 1,
  position:'absolute',
  bottom:0
 },
  txt2:{
    fontSize: 20,
    color:'#ffff',
    textAlign:'center',
    marginBottom:5
  },
 
  btnContainer: {
    flexDirection: 'row',
    flex:1,
  },
    btnText1: {
    fontSize:16,
    backgroundColor: '#99004d',
    width:180,
    height:50,
    paddingTop:12,
    textAlign:'center',
    color:'#ffff'
  },
    btnText2: {
    fontSize:16,
    backgroundColor: '#808080',
    width:180,
    height:50,
    paddingTop:12,
    textAlign:'center',
    color:'#ffff'    
  },
});

export default vendspin;
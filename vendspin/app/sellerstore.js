import React from 'react';

import {TabNavigator} from 'react-navigation';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Setup from './setup';
import {View,Text,InteractionManager,ScrollView
        , ListView, StyleSheet,Image} from 'react-native';

import Row from './Row'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
});
export default class MyStore extends React.Component {

  constructor(props) {

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        super(props);
        this.state = {
          dataSource: ds.cloneWithRows(['row 1', 'row 2']),
          renderPlaceholderOnly: true
        };
      }

  static navigationOptions = {
    label: 'Home',
    header: {visible:false},
    tabBar: {
            label: '',
            icon: ({ tintColor }) => {
                return (
                  <View>
                    <Icon2
                        name='tag'
                        size={27}
                        color={ "#fff" }
                    />
                  </View>  
                );

            }
        }

  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
    <ScrollView>
        <ListView
        style={styles.container}
        dataSource={this.state.dataSource}
        renderRow={(data) => <Row {...this.props}/>}
        onPress={() => navigate('login')}
      />
    </ScrollView>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};


import React from 'react';

import {TabNavigator} from 'react-navigation';

import {Button,Text} from 'native-base';
import {Image} from 'react-native';
import Setup from './setup';

class Tab extends React.Component {
  static navigationOptions = {
    label: 'Home',
    header: {visible:false},
  };

  render() {
    const { navigate } = this.props.navigation;
    return <Button
        title="Go to Setup Tab"
        onPress={() => navigate('setup')}
      >
      <Text>Hello Tab</Text>
      </Button>
   ;
  }
}

const tab = TabNavigator({
  tab: 
      {
        screen: Tab,
      },
  setup: 
        {
          screen: Setup
        },

});

/*
const styles = StyleSheet.create({
  icon: {
    width: 26,
    height: 26,
  },
});

const MyApp = TabNavigator({
  tab: {
    screen: Tab,
  },
  Notifications: {
    screen: MyNotificationsScreen,
  },
}, {
  tabBarOptions: {
    activeTintColor: '#e91e63',
  },
});*/

export default tab;
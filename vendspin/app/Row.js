
import React from 'react';
import { TouchableHighlight,View, Text,Button, StyleSheet, Image } from 'react-native';
import { StackNavigator } from 'react-navigation';

import Login from './login';
import Signup from './signup';
import Dashboardseller from './dashboardseller';
import Tab from './tab';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    marginLeft: 12,
    fontWeight:'bold',
    fontSize: 18
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
});

export default class Row extends React.Component {

static navigationOptions = {
    label: 'Home',
    header: 
    {
      visible:true,

      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    },
  };
  render() {
    const { navigate } = this.props.navigation;
    
    var stores = [];

   for (var i = 0; i < 50; i++) 
        
        {
       stores.push(
        <TouchableHighlight key = {i} 
        onPress={() => navigate('sellerviewstore')}>
            <View style={styles.container}
            >
            <Image 
            source={{ uri: 'http://tlists.com/wp-content/uploads/2016/04/Mobile-phone.jpg'}} style={styles.photo} />
            <Text style={styles.text} >
            {`${'Items'} ${''}`}
            </Text>
            </View>
        </TouchableHighlight>    
      )
        }
    return (
      <View>
      {stores}
      </View>
    );

  }

}



import React from 'react';
import {
          View,
          InteractionManager,
          ScrollView,
       } from 'react-native';

import { Container, Content, Form, Item, Badge,
       Input, Text, Button,StyleProvider,Header,Body,Right,Left,Icon,
       Title,List,ListItem,Thumbnail,ActionSheet,Card,CardItem } from 'native-base';

import LocalImage from './LocalImage';
import Icon2 from 'react-native-vector-icons/FontAwesome';

import Controller from './controller/controller';

var BUTTONS = [
  'Camera',
  'Gallery',
  'Cancel',
];
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;

export default class ViewItems extends React.Component {

  constructor(props) {

        super(props);
        this.controller = new Controller();

        this.state = {
          quantity:"1",
          price:props.navigation.state.params.item.item_price,
          renderPlaceholderOnly: true,
          dataNavigate:props.navigation.state.params.item,
        };

        console.log(JSON.stringify(this.state.dataNavigate.item_description))
      }
  
  isEmptyField()
    {
           if (this.state.quantity=="")
           {
                return true;
           }
           {
                return false;
           }
    }

    async onSubmit()
      {
              if (this.isEmptyField()==true) 
                {
                      return "Please fill Quantity";
                } 
                else 
                {   

                     // await this.controller.cart();

         var cartItems = await this.controller.getCart();  
         
         if(cartItems)
         {
          
          if (typeof(cartItems[this.state.dataNavigate.store_id]) !== 'undefined') 
          {

              var cartTotal = parseInt(cartItems[this.state.dataNavigate.store_id].cartTotal)+parseInt(this.state.price);
              cartItems[this.state.dataNavigate.store_id].cartTotal= cartTotal;
              

               if (typeof(cartItems[this.state.dataNavigate.store_id].items[this.state.dataNavigate.item_id]) !== 'undefined') 
               { 

                

                var qty=parseInt(cartItems[this.state.dataNavigate.store_id].items[this.state.dataNavigate.item_id].qty)+parseInt(this.state.quantity);
                cartItems[this.state.dataNavigate.store_id].items[this.state.dataNavigate.item_id].qty=qty;

               }
               else //when item is not present
               {
                cartItems[this.state.dataNavigate.store_id].items[this.state.dataNavigate.item_id]={
                  qty:this.state.quantity
                };
               }



          }
          else
          {      //when store is not present
                    items={};
                    items[this.state.dataNavigate.item_id]={
                      qty:this.state.quantity,
                    }
                    cartItems[this.state.dataNavigate.store_id] = {
                      cartTotal:this.state.price
                    }
                    cartItems.store_id = this.state.dataNavigate.store_id;
                    cartItems.store_name = this.state.dataNavigate.store_name;
          }



         }
         else
         {          //when cart is not present
                  cartItems={};
                  items={};
                  items[this.state.dataNavigate.item_id]={
                    qty:this.state.quantity,
                  }
                  cartItems[this.state.dataNavigate.store_id] = {
                    cartTotal:this.state.price,
                    items:items
                  }
                  
         }
         
        
          // cartItems.store_name = this.state.dataNavigate.store_name;
          // cartItems.store_id = this.state.dataNavigate.store_id;
                    
         var check = await this.controller.addToCart(cartItems);            

         console.log("\n\n"+JSON.stringify(check)+" FROM STORAGE");
                     
        }
      }


/////////////////////////////////////PlaceHolder////////////////////////////////////
    
  static navigationOptions = {
    header: 
    {
      visible:true,
      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    },
    title: ({state}) => `${state.params.item_name}`,

  };

  async componentDidMount() {
      this.str = await this.controller.getUserStr();
      console.log(this.str +" STRING");
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }

    return (
    <View style={{position:'relative',flex: 1,}}>
          
        <ScrollView style={{flex: 1}}>
                <List>        
                    <Card >
                          <CardItem cardBody>
                              <LocalImage 
                                source={require('./img/Items/camera.jpg')}
                                orignalWidth = {600}
                                orignalHeight = {489}
                                />
                          </CardItem>
                          <CardItem>
                            <Body>
                                <Text>{this.state.dataNavigate.item_description}</Text>
                            </Body>
                        </CardItem>
                   </Card>

                    <ListItem last>
                        <Body>
                              <Item last>
                                  <Input 
                                  keyboardType = 'numeric' 
                                    value={this.state.quantity}
                                   onChangeText={(text) => this.setState({quantity: text})}/>
                                  
                              </Item>

                            <Text style={{marginTop:20,color:'red',fontSize: 18,flex: 1,textAlign:'center',
                                          fontWeight:'bold'}}>Price {this.state.price=this.state.dataNavigate.item_price*this.state.quantity} $</Text>  
                        </Body>
                    </ListItem>
                    <ListItem>
                    <Body>
                    <Button style={{width:170,backgroundColor:'#99004d',
                         alignSelf: 'center',flex: 1,}}
                         onPress={() => this.onSubmit()}
                    >
                        <Text style={{textAlign:'center',flex:1,fontWeight:'bold'}} >Add to Cart</Text>
                  </Button>
                  </Body>
                  </ListItem>

                </List>      
                    
        </ScrollView>   
              </View>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};


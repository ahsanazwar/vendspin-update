import React from 'react';
import {
  Text,
} from 'react-native';

export default class Dashboardseller extends React.Component {
  static navigationOptions = {
    title: ({ state }) => `${state.params.name}'s Profile!`,
  };

  constructor(props) {
    super(props);
  
    this.state = {};
  }
  render() {
    const {state} = this.props.navigation;
    return <Text>{state.params.name}</Text>;
  }
}


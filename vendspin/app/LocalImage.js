
'use strict';
import React, {Component} from 'react';

import {
	Image,
	Dimensions
} from 'react-native';

const LocalImage = ({source,orignalWidth,orignalHeight}) => {
    
		let windowWidth = Dimensions.get('window').width
		let windowChange = (windowWidth)/orignalWidth
		let newWidth = orignalWidth * windowChange
		let newHeight = orignalHeight * windowChange 
	return (
		<Image source={source} style = {{width:newWidth, height:newHeight}}/>
		)


}

export default LocalImage
import React from 'react';

import {
  ScrollView,
  InteractionManager
} from 'react-native';


import { View, Container, Content, Form, Item,
       Input,Toast, Label, Text,ButtonHeader,
       Right, Title, Button, Left,Body,Icon} from 'native-base';

import { RadioGroup } from 'nachos-ui'
import Prompt from 'react-native-prompt';

import { StackNavigator } from 'react-navigation';

import Dashboardseller from './dashboardseller';
import Dashboardbuyer from './dashboardbuyer';

//Globel Array//
var email = "ahsan.azwar@gmail.com";
var password = "12345";
var validateUser = {"EMail":email,"Password":password};

import Controller from './controller/controller';



export default class Login extends React.Component {
  
    constructor(props) 
    {
      super(props);
      this.controller = new Controller();
      this.state = {
        text: '',
        promptVisible: false,
        value: 'Buyer',      
        showToast: false,
        invalidEmail:'',
        eMail:'',
        password:'',
        renderPlaceholderOnly: true
      };
  }

  validateEmail = (email) => 
  {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  isEmptyField()
  {
        if (this.state.eMail=="" || this.state.password=="")
          {
              return true;
          } 
          else
          {
              return false;
          }
  }

  // validateUser()
  // {
  //           if(this.state.eMail == validateUser["EMail"] &&  
  //             this.state.password == validateUser["Password"]) 
  //           {
  //               return true;
  //           }
  //           else
  //           {
  //               return false;   
  //           }
  // }

  async onSubmit() 
  {

    if (this.isEmptyField()==true) 
    {
      Alert.alert("Please Fill All The Fields!");
    }
    else
    {
        if (!this.validateEmail(this.state.eMail)) 
        {
                Alert.alert("Please Input a Valid Email");
        } 
        else
        {
          
          var userProfile = {};
          userProfile = 
          {
              type:this.state.value,
              email:this.state.eMail,
              password:this.state.password,
          };

          var get = await this.controller.login(userProfile);
          console.log(JSON.stringify(get))
          if(get)
          {
            if (get.status == true) 
            {   
                console.log(get.status +" Login confirmation");
                if(get.data.user_type == "Seller")
                {
                  this.props.navigation.navigate('selling',{User:get.data.user_type})
                }
                else if(get.data.user_type == "Buyer")
                {
                 this.props.navigation.navigate('buyerstore',{User:get.data.user_type})
                }
            }
            else
            {
               alert(get.msg);
            } 
          }
          else
          {
            alert("Something went wrong please check your connections");
          }  
      }
    }

    
  };

    static navigationOptions = {
    title: 'Login',
    header:
    {
      tintColor:'#fff',
      style:{backgroundColor:'#99004d'}
    }
  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }

    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }
  
  return (
  <Container style={{backgroundColor:'white'}}>
          <Content scrollEnabled={false}>
          
             <Container style={{flex:1,justifyContent:'center'}}>
             <Form style={{justifyContent:'center'}}>
               <Prompt
                title="Forget Password"
                placeholder="Enter Email"
                visible={ this.state.promptVisible }
                onCancel={ () => this.setState({
                  promptVisible: false,
                }) }
                onSubmit={ (value) => this.setState({
                  promptVisible: false,
                  message: `You said "${value}"`
                }) }/>

                  <RadioGroup
                  onChange={value => this.setState({ value  })}
                  defaultSelected={'Buyer'}
                  options={['Buyer','Seller']}
                  style={[{marginLeft:20,marginBottom:8}]}
                />
                    
                  <Item last>
                        <Input placeholder='Email'
                        onChangeText={(text) => {
                        this.setState({eMail: text});
                      }}/>

                    </Item>
                  <Item last>
                      <Input placeholder="Password" secureTextEntry={true}
                      onChangeText={(text) => this.setState({password: text})}/>
                  </Item>

                  <Text style={{color:'#99003d',textAlign:'right',
                                marginRight:8,marginTop:6}} 
                       onPress={() => this.setState({ promptVisible: true })}>Forget Password</Text>
                     
                  <Button style={{width:170,backgroundColor:'#99004d',marginTop:20,
                         alignSelf: 'center'}}
                         onPress={() => this.onSubmit()}>

                        <Text style={{textAlign:'center',flex:1}} >Login</Text>
                  </Button>
              </Form>
            </Container>
      </Content>      
      </Container>
 )
  }

_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};


'use strict';

var ReactNative = require('react-native');
import Model from '../models/modelInit';

export default class registerModel extends Model
{
   
   constructor() 
   {
     super();
     this.seller_url = super().getSellerUrls();
     this.general_url = super().getGeneralUrls();
   };


   async addItem(data,attr)
    {
      return fetch(this.seller_url.addItem+attr, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
            })
           .then(async (response) => {return response.json()})
           .then(async (res) => 
           { 
            return res;   
           })
           .catch((error) => {
              console.log(error);
            });
    };





    async editItem(data,attr)
    {	
      return fetch(this.seller_url.editItem+attr, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
            })
           .then(async (response) => {return response.json()})
           .then(async (res) => 
           { 
            return res;   
           })
           .catch((error) => {
              console.log(error);
            });
    };


    async deleteItem(data,attr)
    {	
      return fetch(this.seller_url.deleteItem+attr, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
            })
           .then(async (response) => {return response.json()})
           .then(async (res) => 
           { 
            return res;   
           })
           .catch((error) => {
              console.log(error);
            });
    };


    async getStore(data,attr)
    {
      return fetch(this.general_url.getStore+attr)
      .then((response) => response.json())
      .then((getUserData) => {
        return getUserData;
      })
      .catch((error) => {
        console.log(error);
      });

    };

    async getStoreItems(data,attr)
    {
      return fetch(this.general_url.getStore+attr, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
            })
           .then(async (response) => {return response.json()})
           .then(async (res) => 
           { 
            return res;   
           })
           .catch((error) => {
              console.log(error);
            });

    };

}	
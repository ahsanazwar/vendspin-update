/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

var ReactNative = require('react-native');
import Model from '../models/modelInit';

export default class registerModel extends Model
{
   
   constructor() 
   {
     super();
     this.url = super().getRegisterUrls();
   };


   // getStatus
   getStatus()
   {
    return "running registerModel";
   };

   init()
   {
   	
   }

   key()
   {
      return 'profile'
   }







   async postProfile(postData,dataType)
   {
    
    var setUrl;
    if(dataType == "signup")
    { 
      setUrl = this.url.signup;
    }      
    else
    {
      setUrl = this.url.login;
    }
          
	 	return fetch(setUrl, {
	            method: 'POST',
	            headers: {
	              'Accept': 'application/json',
	              'Content-Type': 'application/json',
	            },
	            body: JSON.stringify(postData)
	          })
	         .then(async (response) => {return response.json()})
	         .then(async (res) => 
	         { 
            return res;	  
	         })
	         .catch((error) => {
	            console.log(error);
	          });
    };

    

    async updateUser(data,attr)
    {
      return fetch(this.url.updateUser+attr, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
            })
           .then(async (response) => {return response.json()})
           .then(async (res) => 
           { 
            return res;   
           })
           .catch((error) => {
              console.log(error);
            });
    };




    async getUser(attr)
    {  

    return fetch(this.url.getUser+attr)
      .then((response) => response.json())
      .then((getUserData) => {
        return getUserData;
      })
      .catch((error) => {
        console.log(error);
      });

    } 

}


 

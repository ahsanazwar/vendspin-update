/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';

export default class modelInit
{
   
   constructor() 
   {
     this.data = {};
     this.url = {
      
      status:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/success/",

      registerModel:
      {
        signup:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/signup/" ,
        login:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/login/",
        getUser:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/getUser/",
        updateUser:"http://s586519957.onlinehome.us/projects/vendspin_ws/updateUser/",
      },

      seller:
      {
        addItem:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/addItem/",
        editItem:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/updateItem/",
        deleteItem:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/deleteItem/",
      },

      buyer:
      {
        getAllStores:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/getAllStores"
      },

      general:
      {
        getStore:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/getStore/",
        getItem:"http://s586519957.onlinehome.us/projects/vendspin_ws/ws/getItem/",
      }

    }
   };


   getRegisterUrls()
   {  
      console.log("\n\n\n\n"+" REGISTER URLS "+"\n\n\n\n")
      return this.url.registerModel;
   };



   getSellerUrls()
   {  
      console.log("\n\n\n\n"+" SELLER URLS "+this.url.seller.addItem+"\n\n\n\n")
      return this.url.seller;
   };


   getBuyerUrls()
   {  
      console.log("\n\n\n\n"+" BUYER URLS "+this.url.buyer.getAllStores+"\n\n\n\n")
      return this.url.buyer;
   };

   getGeneralUrls()
   {
      console.log("\n\n\n\n"+" GENERAL URLS "+"\n\n\n\n")
      return this.url.general;
   };

   // getStatus
   getStatus()
   {
    return "running initModel";
   };

   

}


 

'use strict';

var ReactNative = require('react-native');
import Model from '../models/modelInit';

export default class registerModel extends Model
{
   
   constructor() 
   {
     super();
     this.buyer_url = super().getBuyerUrls();
     this.general_url = super().getGeneralUrls();
   };


   async getAllStores(data,attr)
    {
      return fetch(this.buyer_url.getAllStores+attr, {
              method: 'POST',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(data)
            })
           .then(async (response) => {return response.json()})
           .then(async (res) => 
           { 
            return res;   
           })
           .catch((error) => {
              console.log(error);
            });
    };

}	
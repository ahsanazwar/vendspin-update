/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
'use strict';


var ReactNative = require('react-native');
var {
  AsyncStorage,
} = ReactNative;


export default class storageModel 
{
   
   constructor() 
   {
      
   };


   // getStatus
   getStatus()
   {
    return "running storageModel";
   };




   getJsonString(data)
   {
      return JSON.stringify(data);
   }

   getJsonParse(data)
   {
      return JSON.parse(data);
   }


   // setData
   async setLocalData(key,data)
   {
   		await AsyncStorage.setItem(key, JSON.stringify(data));
   }


   // getData()
   async getLocalData(key)
   {     
         //console.log("\nSTORAGE MODEL "+key+" AsyncStorage get keyName\n");
         try 
         {
   		     var get = await AsyncStorage.getItem(key);
           //console.log("\nSTORAGE MODEL "+get+" AsyncStorage get\n")
           return JSON.parse(get);
         }
         catch(e)
         {
            console.log(e);
         }
   }	


   async merge(key,data)
   {
       AsyncStorage.mergeItem(key, this.getJsonString(data))
   }

   // getLocalDefault
   async getLocalDefault()
   {  
      let UID123_object = 
      {
       name: 'Chris',
       age: 30,
      };
   	  
      await AsyncStorage.setItem('default', this.getJsonString(UID123_object));
      
      try 
      {
          return this.getJsonParse(await AsyncStorage.getItem('default')); 
      }
      catch(error)
      {

      }
   }
  

   async clearData()
   {
      await AsyncStorage.clear();
   }
   
}


 

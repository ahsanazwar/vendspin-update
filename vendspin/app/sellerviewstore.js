import React from 'react';

import {TabNavigator} from 'react-navigation';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Setup from './setup';
import {View,Text,InteractionManager,ScrollView
        , ListView, StyleSheet,Image} from 'react-native';

import { ListItem,List,Thumbnail,Container,Content,Left,Right,Body,CardItem
         ,Button,Card,Item,Input,Icon } from 'native-base';        
import LocalImage from './LocalImage';

import Row from './Row'
import { StackNavigator } from 'react-navigation';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
  },
});
export default class SellerViewStore extends React.Component {

  constructor(props) {

        super(props);
        this.state = {
                title:'',
                description:'',
                price:'',
                quantity:'',
          renderPlaceholderOnly: true
        };
      }

      isEmptyField()
          {
             if (this.state.title=="" && this.state.description==""
                && this.state.price=="" && this.state.quantity=="")
                {
                    return true;
                }
                else
                {
                    return false;
                }
          }
        onSubmit()
      {
          if (this.isEmptyField()==true) 
            {
                  return "Please fill any field for updation";
            } 
            else 
            {
                var userProfile = {};
                 userProfile = {"Title":this.state.title,
                 "Description":this.state.description,"Price":this.state.price,
                 "Quantity":this.state.quantity};
                 return "Updated";
            }
      }

  static navigationOptions = {
    label: 'Home',
    header: {visible:false}
  };

  componentDidMount() {
      InteractionManager.runAfterInteractions(() => {
      this.setState({renderPlaceholderOnly: false});
    });
  }
        static navigationOptions = {
    title: 'View Items',
  };
    render() {
      const { navigate } = this.props.navigation;

    if (this.state.renderPlaceholderOnly) {
      return this._renderPlaceholderView();
    }
    return (
      <Container style={{backgroundColor:'white'}}>
              <Content>

                        <List>        
                    <Card >
                          <CardItem cardBody>
                              <LocalImage 
                                source={require('./img/Items/camera.jpg')}
                                orignalWidth = {600}
                                orignalHeight = {489}
                                />
                          </CardItem>
                   </Card>

                    <ListItem last>
                        <Body>

                              <Item last>
                                  <Input placeholder="Title"
                                  onChangeText={(text) => this.setState({Title: text})}/>
                              </Item>

                              <Item last>
                                  <Input placeholder="Description"
                                  onChangeText={(text) => this.setState({Description: text})}/>
                              </Item>

                              <Item last>
                                  <Input keyboardType = 'numeric' placeholder="Price"
                                  onChangeText={(text) => this.setState({Price: text})}/>
                              </Item>

                              <Item last>
                                  <Input keyboardType = 'numeric' placeholder="Quantity"
                                  onChangeText={(text) => this.setState({Quantity: text})}/>
                              </Item>
                        </Body>
                    </ListItem>
                    <ListItem>
                    <Body>
                    <Button style={{width:170,backgroundColor:'#99004d',
                         alignSelf: 'center',flex: 1,}}
                         onPress={() => alert(this.onSubmit())}
                    >
                        <Text style={{textAlign:'center',flex:1,fontWeight:'bold'}} >Submit</Text>
                  </Button>
                  </Body>
                  </ListItem>

                </List>

                    </Content>
                </Container>
  );
  }
_renderPlaceholderView() {
    return (
      <View>
      </View>

    );
  }
};

